CarService v1.5.1 - Copyright (C)  Zeljko Stevanovic aka fluxmeister, 2019. All Rights Reserved.

Java program that handles with CarService entities, such are vehicles, customers, companies, 
work orders, work schedules, appointments and servicers.


REQUIREMENTS

	JRE


TESTED PLATFORMS

	Windows 


HISTORY
    v1.5.1	- added RadniTermin (Work Term)
	v1.5.0	- added database support (MySQL)
    v1.2	- reading .xlsx files
	v1.1	- displaying work orders for a working term,
	v1.0	- first public release


LATEST VERSION

	Latest version is always available from:

	Sources (GIT)
		https://gitlab.com/fluxmeister/autoservis


fluxmeister <fluxmeister@hotmail.rs> <www.vektorwebsolutions.com>

27-September-2019
